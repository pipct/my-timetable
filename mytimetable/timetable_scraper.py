# Copyright (C) 2017  Jamie McClymont, Rhys Davies, and Nick Webster
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from mytimetable.datatypes import Trimester, Activity, ActivityInstanceSet

from bs4 import BeautifulSoup
import datetime as dt
import re
from collections import defaultdict
from mytimetable.named_week_ranges import named_week_ranges

day_re = re.compile(r"^(\d+):(\d+)-(\d+):(\d+)$")
week_re = re.compile(r"^Week (\d+)$")


def parseTimeRange(elem):
    parts = [int(n) for n in day_re.match(elem.get_text()).groups()]

    return (dt.time(parts[0], parts[1]),
            dt.time(parts[2], parts[3]))


def parseWeeks(elem):
    if elem['class'][0] == "WeeksDescription":
        if elem(class_='SingleWeek') != []:
            line = elem(class_='SingleWeek')
            week_num = int(week_re.match(line[0].get_text()).group(1))
            return [(week_num, week_num)]
        return [(
            int(week_re.match(start.get_text()).group(1)),
            int(week_re.match(end.get_text()).group(1)))
            for start, end
            in zip(elem(class_="RangeWeekFrom"), elem(class_="RangeWeekTo"))]
    elif elem['class'][0] == "NamedWeekPattern":
        return named_week_ranges[elem.get_text()]


def parseName(name):
    """Take unformatted name and remove dashes and numbers.

    Args:
        name (str): The unformatted name
    Returns:
        str: Formatted version of name
    """
    parts = name.split('-')
    nparts = []
    for part in parts:
        if '/' in part:
            part = part.split('/')[0]
        if not part.isdigit():
            nparts.append(part)
    return " ".join(nparts)


def scrape(html, trimester_n):
    doc = BeautifulSoup(html, 'html.parser')

    trimester = doc.select('div.Timetable')[trimester_n - 1]
    trimester_obj = Trimester(name=trimester.span.get_text(),
                              activity_instance_sets=defaultdict(lambda: []))

    days = {}

    for day, dayinfo in zip(range(1, 8),
                            trimester.select("ul div.TimetableDay")):
        days[day] = []

        for activity in dayinfo.select("ul.ActivitiesOnDay li div.Activity"):
            days[day].append((
                parseTimeRange(activity(class_="TimetableItemTime")[0]),
                parseWeeks((activity(class_="WeeksDescription") + activity(class_="NamedWeekPattern"))[0]),
                Activity(
                    name=parseName(activity(class_="Name")[0].get_text()),
                    location=activity(class_="Activity_LocationsName")[0].get_text(),
                    zone=activity(class_="Activity_ZoneName")[0].get_text(),
                    time=activity(class_="StartDescription")[0].get_text()
                )
            ))

    for dow, dowacts in days.items():
        for (time_range, weeks, activity) in dowacts:
            trimester_obj.activity_instance_sets[activity].append(ActivityInstanceSet(time_range, weeks, dow))

    return trimester_obj

# import code; code.interact(local=locals())
