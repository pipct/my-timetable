FROM alpine AS build

EXPOSE 8000

RUN apk add --no-cache build-base libffi-dev openssl-dev libxml2-dev libxslt-dev py-pip python3-dev cargo

RUN python3 -m venv /venv

RUN /venv/bin/pip install -U pip
RUN /venv/bin/pip install poetry

WORKDIR /app

ADD . /app

RUN /venv/bin/poetry config virtualenvs.create false
RUN /venv/bin/poetry install --no-dev

FROM alpine

RUN apk add --no-cache uwsgi-python python3

COPY --from=build venv venv

WORKDIR /app

ADD . /app

ENTRYPOINT exec /usr/sbin/uwsgi --socket "0.0.0.0:8000" \
               "--uid" "uwsgi" \
               "--plugins" "python" \
               "--protocol" "uwsgi" \
               "--venv" "/venv" \
               "--file" "server.py" \
               "--callable" "server"
